export interface GasAlert {
  threshold: number;
  channelId: string;
  userId: string;
}