export interface GasPrices {
  fast: number;
  average: number;
  slow: number;
  values?: number[];
}
