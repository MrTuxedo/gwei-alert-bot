import 'dotenv/config.js';
import {
  Events,
  Message,
  EmbedBuilder,
  User
} from "discord.js";
import { DiscordClient } from '../discordClient';

module.exports = {
  name: Events.MessageCreate,
  async execute(client: DiscordClient, message: Message) {
    // Don't respond to bots or replies
    if (message.author.bot) return;
    if (message.mentions.has(client.user as User, {
      ignoreRoles: true,
      ignoreRepliedUser: true,
      ignoreEveryone: true
    })) {
      const embeds = [
        new EmbedBuilder()
          .setColor('#008000')
          .setTitle('Available slash commands')
          .setDescription(
`/gas
/alert [GWEI]
/alert-delete
/pending-alert`
          )
      ]
      message.reply({embeds});
    }
  }
};