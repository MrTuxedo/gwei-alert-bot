import 'dotenv/config.js';
import fs from 'node:fs';
import path from 'node:path';
import { ApplicationCommand, REST, Routes } from 'discord.js';

const clientId = process.env.DISCORD_CLIENT || "";
const token = process.env.DISCORD_BOT_TOKEN || "";

const commands: ApplicationCommand[] = [];
// Grab all the command files from the commands directory you created earlier
const commandsPath = path.join(__dirname, './commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

// Grab the SlashCommandBuilder#toJSON() output of each command's data for deployment
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	commands.push(command.data.toJSON());
}

// Construct and prepare an instance of the REST module
const rest = new REST({ version: '10' }).setToken(token);

// and deploy your commands!
export const deployCommands = async () => {
	try {
		// The put method is used to fully refresh all commands in the guild with the current set
		const data = await rest.put(
			Routes.applicationCommands(clientId),
			{ body: commands },
		) as ApplicationCommand[];

		console.log(`Successfully reloaded ${data.length} global application (/) commands.`);

	} catch (error) {
		// And of course, make sure you catch and log any errors!
		console.error('Error setting application commands: \n', error);
	}
};