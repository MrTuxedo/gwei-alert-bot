import { SlashCommandBuilder, ChatInputCommandInteraction } from 'discord.js';
import { handleAlertDeleteCommand } from '../handlers';

module.exports = {
    data: new SlashCommandBuilder()
    .setName('alert-delete')
    .setDescription('Remove your gwei alert threshold'),
  
    async execute(interaction: ChatInputCommandInteraction) {
        return await handleAlertDeleteCommand(interaction);
    }
}