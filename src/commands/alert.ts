import { SlashCommandBuilder, ChatInputCommandInteraction } from 'discord.js';
import { handleGasAlertCommand, handleGasCommand, handleGasPendingCommand } from '../handlers';

module.exports = {
    data: new SlashCommandBuilder()
    .setName('alert')
    .setDescription('Set gas alert for gwei threshold')
    .addStringOption(option =>
        option.setName('gwei')
          .setDescription('gwei threshold')
          .setRequired(true)),
  
    async execute(interaction: ChatInputCommandInteraction) {
        return await handleGasAlertCommand(interaction);
    }
}