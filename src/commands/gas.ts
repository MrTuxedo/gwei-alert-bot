import { SlashCommandBuilder, ChatInputCommandInteraction } from 'discord.js';
import { handleGasCommand } from '../handlers';

module.exports = {
  data: new SlashCommandBuilder()
    .setName('gas')
    .setDescription('Get current gas values'),

  async execute(interaction: ChatInputCommandInteraction) {
    return await handleGasCommand(interaction);
  }
};