import { SlashCommandBuilder, ChatInputCommandInteraction } from 'discord.js';
import { handleGasPendingCommand } from '../handlers';

module.exports = {
    data: new SlashCommandBuilder()
    .setName('pending-alert')
    .setDescription('Get your gwei alert threshold'),
  
    async execute(interaction: ChatInputCommandInteraction) {
        return await handleGasPendingCommand(interaction);
    }
}