# Gwei Alert Bot

## Introduction

This repo contains Docker build files for a Discord bot built with Node.js and utilizes a composed Redis container for storage.

## Requirements

- Docker + Docker Compose
- A Discord bot token
- A EVM RPC endpoint URL (websocket enabled) OR a local node running at `localhost:8545` (using docker host network mode)

## Usage

1. Clone the repository to your local machine

```bash
git clone https://git.ssh.surf/MrTuxedo/gwei-alert-bot.git
```

2. Create a .env file in the root of the project and add the following environment variables:

```makefile
RPC_URL=<your rpc url>
DISCORD_BOT_TOKEN=<your discord bot token>
DISCORD_CLIENT=<your discord client ID>
```

3. Run the Docker container

```bash
docker compose up -d
```

## Rebuilding with new changes

1. Stop the running bot and remove container by same name

```bash
docker compose down
```

2. Pull the new work

```bash
git pull
```

3. Build and Run

```bash
docker compose up -d --build
```

## Environment Variables

The following environment variables must be set in either `.env` or `docker-compose.yml` in order for the bot to function properly:

- RPC_URL: Your RPC url
- DISCORD_BOT_TOKEN: the Oauth2 token for your Discord bot.
- DISCORD_CLIENT: the client ID for your Discord bot. (app ID)

### License

[WTFPL](./LICENSE)